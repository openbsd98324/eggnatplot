#! /bin/sh
# the next line restarts using wish \
exec wish "$0" "$@"
#
#########################################################################
# PROGRAM: Eggshape
# 
# This program perform the calculation of the 2d profiel shape of 
# bird eggs using the Baker equation 
# y = T*(1+x)^(1/1+lam)(1-x)^(la/(1+lam)). 
# (D. E. Baker. The Auk 119(4):1179â1186, 2002.)
# The shape of the curve is defined by the two parameters
#  asymmetry  : A=lam-1, 
#  ellipticity: E=1/T-1, 
# 
# The program is provided with a small database of selected European birds
# with parameters (A, and E) obtained from the paper:
# M. C. Stoddard et al. Science 356, 1249â1254 (2017).
# ######################################################################
# DEVELOPED BY
# (c) Danilo Roccatano 2018
# #
# VERSION: 1.0 
# # 
#########################################################################
#
# root is the parent window for this user interface
 
package require Tk
 
wm title . ""
tk_setPalette cyan3  
 
# Define some variables
# this treats "." as a special case
set root "."
 
set base ""
set maxX 500
set maxY 500
set width  0
set height 0
set midX   0
set midY   0
 
set rmin -5.0
set rmax 5.0 
set ymax   2.
set ymin  -2.
set T 1.0
set np 300
set lam 1.0
set pA  0.0
set pE  0.0
set pL  1.0
set vol 0.0
set surf 0.0
set nc  0 
set ex1 0.5 
set ex2 0.5 
 
 
set COLORS { black gray75 gray50 white darkred red DarkMagenta
      magenta green4 green Gold4 yellow navy blue turquoise4 cyan
}
set nCOLORS [llength $COLORS]
 
set PARAM { 
" 0.1636  0.3456  1.8400" " 0.2897  0.5311  4.3587" " 0.1519  0.3363  1.6534" " 0.0769  0.3573  4.8971" \
" 0.0679  0.4191  6.0883" " 0.2182  0.4966  4.8785" " 0.4331  0.6112  4.9874" " 0.0924  0.2442  3.5432" \
" 0.1486  0.3406  1.6513" " 0.1706  0.4153  2.8629" " 0.2586  0.4590  5.9276" " 0.2121  0.3807  4.0492" \
" 0.1938  0.3483  2.6981" " 0.1117  0.3466  2.8217" " 0.1611  0.3540  4.0471" " 0.1398  0.3118  4.2910" \
" 0.1265  0.3308  1.9853" " 0.1291  0.4044  6.0698" " 0.0933  0.6039  9.1690" " 0.1207  0.2856  1.6177" \
" 0.1730  0.4024  5.1333" " 0.2199  0.4778  5.8653" " 0.1145  0.3064  5.6094" " 0.1430  0.3602  1.9425" \
" 0.1612  0.6657  9.7472" " 0.2706  0.4178  3.4035" " 0.0700  0.3476  8.9076" " 0.1708  0.4462  2.4311" \
" 0.0413  0.4703  4.7640" " 0.0713  0.3530  2.8341" " 0.1013  0.2954  1.9652" " 0.1356  0.3123  1.8190" \
" 0.1063  0.4736  4.3948" " 0.4287  0.5156  5.4411" " 0.2064  0.6456  7.9108" " 0.1883  0.3342  1.6722" \
" 0.2013  0.4230  3.0368" " 0.1449  0.3712  1.9325" " 0.1766  0.3422  1.7096" " 0.2146  0.4097  1.5982" \
" 0.0181  0.1453  2.2946" " 0.1789  0.3115  1.7843" " 0.0881  0.3517  4.5562" " 0.1575  0.3635  2.1076" \
" 0.2111  0.4330  2.4754" " 0.1788  0.3840  4.6936" " 0.1260  0.2847  5.9398" " 0.2204  0.3594  1.9553" \
" 0.1340  0.5066  6.9952" " 0.2166  0.4745  4.8174" " 0.3473  0.3799  3.0342" " 0.1219  0.3583  2.6591" \
" 0.4336  0.6453  7.8649" " 0.1011  0.4011  4.9665" " 0.1621  0.4149  2.0049" " 0.2208  0.3937  4.0903" \
" 0.1202  0.5178  7.7142" " 0.2068  0.3951  1.6354" " 0.1481  0.3984  5.7557" " 0.2035  0.4238  4.2014" \
" 0.1515  0.3532  1.9394" " 0.1237  0.3123  1.4878" " 0.1237  0.3092  2.0214" " 0.1694  0.3551  2.0225" \
" 0.2276  0.3787  2.2665" " 0.0704  0.2610  5.6364" " 0.2244  0.4991  5.5796" " 0.2473  0.5875 10.1558" \
" 0.2173  0.4312  5.7221" " 0.1075  0.2936  4.0485" " 0.0785  0.2403  2.1945" " 0.2528  0.4691  5.1864" \
" 0.1964  0.2980  2.9223" " 0.2141  0.4863  2.1700" " 0.2068  0.5117  6.5706" " 0.1935  0.4223  2.9421" \
" 0.2508  0.5120  2.4391" " 0.2157  0.4011  4.1283" " 0.0731  0.3420  1.3861" " 0.2019  0.4985  3.8930" \
" 0.2370  0.3763  3.5900" " 0.2505  0.4659  5.3474" " 0.1753  0.4578  2.2639" " 0.3940  0.4537  6.6281" \
" 0.2053  0.4100  2.2884" " 0.2819  0.4683  2.5986" " 0.2275  0.5119  4.0186" " 0.3971  0.4786  3.4468" \
" 0.1670  0.3401  1.9334" " 0.0937  0.2717  5.8952" " 0.0560  0.3363  3.2469" " 0.0728  0.3321  7.0769" \
" 0.1654  0.4967  7.3276" " 0.0384  0.1732  2.5503" " 0.0632  0.3883  3.0569" " 0.2016  0.4608  2.4960" \
" 0.0871  0.3170  1.5199" " 0.1920  0.3988  2.9301" " 0.2561  0.4528  4.3439" " 0.1258  0.3374  2.2316" \
" 0.2646  0.4316  5.1410" " 0.0729  0.3895  5.4852" " 0.1636  0.6319  7.9350" " 0.1655  0.3418  1.9098" \
" 0.1922  0.6018 10.6605" " 0.2681  0.4601  7.3257" " 0.1072  0.4256  5.0908" " 0.1518  0.3274  1.3981" \
" 0.1192  0.3058  7.7318" " 0.2662  0.4650  3.1993" " 0.4107  0.5328  5.1358" " 0.0966  0.3842  6.0448" \
" 0.0674  0.4558  6.5418" " 0.1114  0.3186  5.9863" " 0.1257  0.3610  1.7474" " 0.0793  0.4923  8.4211" \
" 0.0995  0.4382  5.0450" " 0.1660  0.6084  8.5517" " 0.1620  0.4227  2.2170" " 0.2747  0.4073  6.7837" \
" 0.1550  0.3224  1.7510" " 0.2803  0.4860  7.8012" " 0.1712  0.5955  8.8469" " 0.2014  0.4624  3.2817" \
" 0.1502  0.3648  1.9337" " 0.1099  0.3489  2.3090" " 0.0753  0.4232  6.0670" " 0.2448  0.3810  3.6486" \
" 0.4142  0.4796  3.0472" " 0.1830  0.3411  1.9338" " 0.1736  0.4360  2.3101" " 0.3232  0.5250  5.3752" \
" 0.2306  0.3719  2.8684" " 0.1322  0.3838 10.1228" " 0.4502  0.6791  8.1741" " 0.1233  0.2796  5.9904" \
" 0.1180  0.3655  5.6550" " 0.1872  0.4567  2.4508" " 0.0436  0.2664  3.9818" " 0.1078  0.2998  4.6828" \
" 0.1509  0.3589  2.2312" " 0.2685  0.4755  7.2042" " 0.1022  0.2116  3.9517" " 0.1435  0.3149  1.8003" \
" 0.1240  0.4339  2.5969" " 0.1693  0.4178  2.1393" " 0.3048  0.4992  5.2708" " 0.2536  0.5312  7.2188" \
" 0.1936  0.4469  1.8936" " 0.1276  0.3557  1.8907" " 0.1648  0.3566  2.0814" " 0.1898  0.4273  3.3777" \
" 0.1704  0.4179  3.2388" " 0.2566  0.3930  3.1152" " 0.1750  0.5281  7.3883" " 0.2552  0.3851  5.5084" \
" 0.1270  0.2896  5.2158" " 0.1631  0.3921  2.0766" " 0.3433  0.4373  4.5780" " 0.2444  0.4174  5.4455" \
" 0.0626  0.3483  3.1600" " 0.0394  0.3307  3.1788" " 0.4847  0.5026  2.9011" " 0.2393  0.4749  6.8727" \
" 0.0999  0.4449  5.6243" " 0.4213  0.4928  4.0009" " 0.1647  0.4020  4.6032" " 0.0733  0.3355  3.6788" \
" 0.0661  0.3489  2.1656" " 0.0831  0.4704  3.7323" " 0.0385  0.3898  3.7977" " 0.2000  0.3722  4.3342" \
" 0.3302  0.4210  2.9137" " 0.0437  0.2482  4.0689" " 0.1272  0.3962  5.2295" " 0.2764  0.4564  5.5264" \
" 0.1346  0.3397  1.4509" " 0.1507  0.4798  6.9727" " 0.1739  0.4544  3.4812" " 0.0980  0.4012  5.7726" \
" 0.1952  0.4854  6.1967" " 0.0453  0.3205  1.5182" " 0.1421  0.3781  1.8426" " 0.1727  0.3820  1.9812" \
" 0.1129  0.3755  1.8011" " 0.1012  0.2717  3.9778" " 0.1740  0.4116  3.0644" " 0.1548  0.4251  4.4020" \
" 0.1128  0.3173  1.9154" " 0.0935  0.5440 11.4329" " 0.0980  0.4089  5.2569" " 0.1381  0.3555  2.4496" \
" 0.2040  0.4651  2.3117" " 0.0936  0.2777  1.9139" " 0.1105  0.4458  3.4690" " 0.1104  0.2708  1.8447" \
" 0.2301  0.4354  5.7429" " 0.1690  0.3578  1.5899" " 0.4288  0.4514  3.7690" " 0.1551  0.4935  1.5784" \
" 0.1175  0.2855  5.3360" " 0.1632  0.2604  4.2325" " 0.1609  0.3381  1.8467" " 0.0444  0.2753  2.9947" \
" 0.0860  0.2659  1.9534" " 0.0985  0.4591  4.3253" " 0.0285  0.4855  4.6293" " 0.1121  0.5340  8.3231" \
" 0.1079  0.4264  5.5042" " 0.2115  0.3634  6.2088" " 0.1395  0.3422  3.2323" " 0.2815  0.4829  6.3065" \
" 0.0875  0.3089  5.3435" " 0.1149  0.3732  5.2614" " 0.1403  0.3239  1.5479" " 0.2355  0.5033  4.7772" \
" 0.2880  0.5818  7.5757" " 0.1681  0.4001  2.1880" " 0.1625  0.3934  4.3524" " 0.1618  0.3788  2.2564" \
" 0.1235  0.4837  6.5329" " 0.1420  0.3060  1.5993" " 0.1917  0.4367  2.0757" " 0.1179  0.4951  6.3122" \
" 0.2113  0.4077  5.4057" " 0.1229  0.5738  5.6156" " 0.0770  0.3912  3.1095" " 0.3441  0.4578  3.0190" \
" 0.1132  0.4513  2.1730" " 0.2051  0.6573  7.2179" " 0.3984  0.4099  4.2849" " 0.1867  0.3536  2.7118" \
" 0.1758  0.3581  1.9238" " 0.1280  0.3826  1.8031" " 0.1499  0.3781  2.0967" " 0.1812  0.3863  2.9527" \
" 0.2452  0.4527  6.0373" " 0.0447  0.4049  5.5571" " 0.3354  0.4179  3.4851" " 0.1126  0.2689  1.9635" \
" 0.0623  0.3308  3.8068" " 0.1873  0.3837  3.8002" " 0.1679  0.3798  2.4202" " 0.2282  0.5707  3.2128" \
" 0.2341  0.4152  4.1273" " 0.0988  0.2700  5.7285" " 0.0802  0.3658  6.2467" " 0.4270  0.5061  4.3012" \
" 0.1102  0.3386  2.3901" " 0.2552  0.4106  4.4835" " 0.1865  0.4022  1.7483" " 0.2337  0.6112  7.8053" \
" 0.0743  0.3555  1.7890" " 0.1854  0.3838  2.2215" " 0.0898  0.4481  6.2056" " 0.0653  0.2517  3.0175" \
" 0.1587  0.3662  1.7430" " 0.3195  0.4628  3.3488" " 0.1936  0.7050  6.6986" " 0.1542  0.3826  2.1190" \
" 0.3291  0.4245  3.9478" " 0.0661  0.2772  4.0137" " 0.0976  0.4350  5.5683" " 0.1929  0.4492  3.0891" \
" 0.2951  0.6100  3.2005" " 0.2268  0.3613  1.7248" " 0.1488  0.3408  1.6882" " 0.2211  0.4560  2.3513" \
" 0.1103  0.3141  1.9360" " 0.1176  0.5360  4.4575" " 0.2410  0.4097  5.4963" " 0.3219  0.4563  3.9769" \
" 0.1972  0.4048  2.2715" " 0.1309  0.3258  1.9919" " 0.1610  0.3278  2.6754" " 0.2401  0.4734  5.1725" \
" 0.1522  0.4208  3.1632" " 0.0808  0.2391  4.0355" " 0.1776  0.4319  3.3752" " 0.2261  0.4186  1.8910" \
" 0.3112  0.3841  3.1437" " 0.0295  0.2923  3.6921" " 0.1611  0.3358  4.9116" " 0.0622  0.3166  2.8192" \
" 0.1744  0.3748  2.2639" " 0.1487  0.3553  2.2793" " 0.2226  0.4358  1.9450" " 0.1580  0.3432  1.6600" \
" 0.1715  0.3847  2.0899" " 0.0905  0.3759  4.5389" " 0.1475  0.3024  1.5931" " 0.1333  0.2656  2.2915" \
" 0.1643  0.4065  2.7919" " 0.1375  0.3460  2.0044" " 0.1506  0.3602  1.9217" " 0.3225  0.4119  4.4447" \
" 0.1403  0.3670  2.2671" " 0.1715  0.4803  7.1403" " 0.1925  0.6200 13.6912" " 0.1657  0.3622  2.0965" \
" 0.3913  0.4656  5.7441" " 0.0651  0.4380  7.6432" " 0.1633  0.3470  1.9996" " 0.2200  0.6530  8.6519" \
" 0.1462  0.3542  2.1499" " 0.1011  0.3721  3.6912" " 0.1009  0.3770  5.0639" " 0.1382  0.5313  7.5785" \
" 0.1394  0.5508  2.2989" " 0.1349  0.2862  7.8854" " 0.1427  0.3483  2.0902" " 0.1400  0.3286  1.7975" \
" 0.1225  0.5273 11.6695" " 0.1109  0.3930  5.3517" " 0.3971  0.4308  5.3917" " 0.1188  0.2981  1.5044" \
" 0.1439  0.3748  2.2050" " 0.0654  0.4041  4.0881" " 0.1433  0.3865  2.5463" " 0.1347  0.3131  1.5944" \
" 0.2332  0.2904  4.2483" " 0.1130  0.3484  2.0917" " 0.0410  0.3201  3.1167" " 0.1766  0.4655  2.6237" \
" 0.1593  0.3466  1.7847" " 0.1392  0.3311  2.0694" " 0.1299  0.3227  1.7420"
}
 
set CNAMES { 
    "Acadian flycatcher " "Aleutian tern " "Am. Yellow warbler " "American bittern " \
"American black duck " "American coot " "American golden plover " "American kestrel " \
"American redstart " "American robin " "Arctic skua " "Arctic tern " \
"Azure-winged magpie " "Baillon's crake " "Barbary partridge " "Barn owl " \
"Barred warbler " "Barrow's goldeneye " "Bewick's swan " "Black and white warbler " \
"Black grouse " "Black guillemot " "Black kite " "Black redstart " \
"Black swan " "Black tern " "Black vulture " "Black wheatear " \
"Black-bellied sandgrouse " "Black-billed cuckoo " "Black-eared wheatear " "Black-faced bunting " \
"Black-necked grebe " "Black-tailed godwit " "Black-throated diver " "Black-throated green warbler " \
"Blackbird " "Blackcap " "Blackpoll warbler " "Blue tit " \
"Blue-cheeked bee-eater " "Blue-headed wagtail " "Blue-winged teal " "Bobolink " \
"Bohemian waxwing " "Bonaparte's gull " "Booted eagle " "Brambling " \
"Brent goose " "Bridled tern " "Broad-billed sandpiper " "Brown thrasher " \
"Brunnich's guillemot " "Bufflehead " "Bullfinch " "Bulwer's petrel " \
"Canada goose " "Cape May warbler " "Capercaillie " "Carrion crow " \
"Chaffinch " "Chiffchaff " "Cirl bunting " "Citrine wagtail " \
"Common bulbul " "Common buzzard " "Common coot " "Common crane " \
"Common gull " "Common kestrel " "Common kingfisher " "Common noddy " \
"Common quail " "Common rosefinch " "Common scoter " "Common starling " \
"Common swift " "Common tern " "Common waxbill " "Corncrake " \
"Cream-coloured courser " "Crested auklet " "Crested lark " "Curlew " \
"Desert lark " "Dipper  " "Dotterel " "Dunlin " \
"Dunnock  " "Eagle owl " "Eastern turtle dove " "Egyptian vulture " \
"Eider " "European bee-eater " "European nightjar " "Evening grosbeak " \
"Fan-tailed warbler " "Fieldfare " "Forster's tern " "Fox sparrow " \
"Franklin's gull " "Gadwall " "Gannet " "Garden warbler " \
"Giant petrel " "Glaucous gull " "Glossy ibis " "Goldcrest " \
"Golden eagle " "Golden oriole " "Golden plover " "Goldeneye " \
"Goosander " "Goshawk " "Grasshopper warbler " "Great bustard " \
"Great crested grebe " "Great northern diver " "Great reed warbler " "Great skua " \
"Great tit " "Greater black-backed gull " "Greater flamingo " "Green woodpecker " \
"Greenfinch " "Grey catbird " "Grey heron " "Grey partridge " \
"Grey phalarope " "Grey wagtail " "Grey-cheeked thrush " "Grey-headed gull " \
"Grey-headed woodpecker " "Griffon vulture " "Guillemot " "Gyrfalcon " \
"Harlequin " "Hawfinch " "Hawk owl " "Hen harrier " \
"Hermit thrush " "Herring gull " "Hobby " "Hooded warbler " \
"Hoopoe " "House sparrow " "Hudsonian godwit " "Iceland gull " \
"Icterine warbler " "Indigo bunting " "Isabelline wheatear " "Jackdaw " \
"Jay " "Kentish plover " "King eider " "Kittiwake " \
"Lanner " "Lapland bunting " "Lapwing " "Laughing gull " \
"Leach's storm petrel " "Least bittern " "Least sandpiper " "Lesser black-backed gull " \
"Lesser scaup " "Lesser yellowlegs " "Little auk " "Little bittern " \
"Little bunting " "Little grebe " "Little green heron " "Little gull " \
"Little ringed plover " "Long-eared owl " "Long-tailed duck " "Long-tailed skua " \
"Long-tailed tit " "Magnificent frigate bird " "Magpie " "Mallard " \
"Manx shearwater " "Marsh tit " "Marsh warbler " "Meadow pipit " \
"Melodious warbler " "Merlin " "Mistle thrush " "Moorhen " \
"Moussier's redstart " "Mute swan " "Night heron " "Northern mockingbird " \
"Northern oriole " "Northern waterthrush " "Nutcracker " "Orphean warbler " \
"Oystercatcher " "Parula warbler " "Pectoral sandpiper " "Penduline tit " \
"Peregrine " "Pheasant " "Pied flycatcher " "Pied kingfisher " \
"Pied wheatear " "Pied-billed grebe " "Pin-tailed sandgrouse " "Pink-footed goose " \
"Pintail " "Pomarine skua " "Pratincole " "Puffin " \
"Purple heron " "Purple swamphen " "Radde's warbler " "Raven " \
"Razorbill " "Red crossbill " "Red grouse " "Red-backed shrike " \
"Red-breasted merganser " "Red-breasted nuthatch " "Red-eyed vireo " "Red-footed booby " \
"Red-knobbed coot " "Red-necked grebe " "Red-necked nightjar " "Red-necked phalarope " \
"Red-rumped swallow " "Red-throated diver " "Redshank " "Redwing " \
"Reed bunting " "Reed warbler " "Richard's pipit " "Ring ouzel " \
"Ring-billed gull " "Ring-necked duck " "Ringed plover " "Robin " \
"Rock (feral) dove " "Rook " "Rose-breasted grosbeak " "Rose-coloured starling " \
"Roseate tern " "Rough-legged buzzard " "Ruddy duck " "Ruff " \
"Rufous-sided towhee " "Sabine's gull " "Sand martin " "Sandhill crane " \
"Sardinian warbler " "Scarlet tanager " "Scaup " "Scops owl " \
"Sedge warbler " "Semipalmated plover " "Shag " "Shore lark " \
"Short-billed dowitcher " "Short-eared owl " "Shoveler " "Siberian jay " \
"Siberian thrush " "Siberian tit " "Siskin " "Sky lark " \
"Slate-coloured junco " "Slavonian grebe " "Slender-billed gull " "Snipe " \
"Snow bunting " "Song sparrow " "Song thrush " "Sooty tern " \
"Sora " "Sparrowhawk " "Spotted crake " "Spotted flycatcher " \
"Spotted sandpiper " "Stock dove " "Stone curlew " "Storm petrel " \
"Summer tanager " "Swainson's thrush " "Swallow " "Taiga treecreeper " \
"Tawny pipit " "Teal " "Tennessee warbler " "Three-toed woodpecker " \
"Tickell's thrush " "Tree pipit " "Tree sparrow " "Upland sandpiper " \
"Veery " "Velvet scoter " "Wandering albatross " "Wheatear " \
"Whimbrel " "White stork " "White wagtail " "White-billed diver " \
"White-crowned sparrow " "White-faced storm petrel " "White-faced whistling duck " "White-fronted goose " \
"White-rumped swift " "White-tailed eagle " "White-throated sparrow " "Whitethroat " \
"Whooper swan " "Wigeon " "Willet " "Willow warbler " \
"Wood lark " "Wood pigeon " "Wood thrush " "Wood warbler " \
"Woodcock " "Wryneck " "Yellow-billed cuckoo " "Yellow-headed blackbird " \
"Yellow-rumped warbler " "Yellowhammer " "Yellowthroat "
}
   #define global variables
 
global width height 
global PARAM pA pE pL vol surf np T ex1 ex2
global COLORS nCOLORS nc
 
#############################################################################
## Procedures
#############################################################################
 
proc AssignParam {} { 
    global PARAM  pA  pE  pL vol nc ex1 ex2 T
    set ind [.lb curselection] 
    set temp [lindex $PARAM $ind]
    set pval [split $temp  " "]
    set pA [lindex $pval 1   ]
    set pE [lindex $pval 3   ]
    set pL [lindex $pval 5   ]
    set nc [expr $nc+1]
 
    set lam [expr $pA+1.0]
    set T   [expr 1.0/($pE+1.0)]
    set ex1 [expr 1.0/(1.0+$lam)]
    set ex2 [expr $lam/(1.0+$lam)]
 
    DrawFn .cv 
} 
 
proc f {x} {
#
    global ex1 ex2 T 
    set x1 [expr 1.0+$x]
    set x2 [expr 1.0-$x]
    set y [expr ($T*pow($x1,$ex1)*pow($x2,$ex2))]
    return [expr pow($y,2.0)]
}
 
proc df {x} {
#
    global ex1 ex2 T 
    set x1 [expr 1.000001+$x]
    set x2 [expr 1.000001-$x]
    set y [expr ($T*pow($x1,$ex1)*pow($x2,$ex2))]
    set dy  [expr $T*($ex1*pow($x1,$ex1-1.0)*pow($x2,$ex2)-$ex2*pow($x1,$ex1)*pow($x2,$ex2-1.0))]
    return  [expr $y*sqrt(1.0+pow($dy,2.0))]
}
 
 
proc Simpson  {f} {
    global np
    set delta [expr {2.0 / $np}]
    set total 0.0
    for {set i 0} {$i < $np} {incr i} {
        set x1 [expr {-1.0+$i * $delta}]
        set x2 [expr {$x1 + $delta}]
        set mid [expr {($x1 + $x2) / 2.0}]
        set simp [expr {([$f $x1] + 4*[$f $mid] + [$f $x2]) / 6.0}]
        set total [expr {$total + $delta * $simp}]
    }
    return $total
}
 
proc CalcVol {} {
    global pL 
    set pi 3.14159265359
    set coeff [expr $pi*pow($pL,3.0)/8.0]
    set res [expr [Simpson f]]
    return  [expr $coeff*[Simpson f]]
}
proc CalcSur {} {
    global pL 
    set pi 3.14159265359
    set coeff [expr $pi*pow($pL,2.0)/2.0]
     
    return  [expr $coeff*[Simpson df]]
}
 
proc GenSTL {} {
    global np
    global ex1 ex2 T 
 
    set nrot 20
    set dthe  0.3141
    set delta [expr {2.0 / $np}]
 
    for {set i 0} {$i < $np} {incr i} {
      set x [expr {-1.0+$i * $delta}]
      set x1 [expr 1.0+$x]
      set x2 [expr 1.0-$x]
      set y [expr ($T*pow($x1,$ex1)*pow($x2,$ex2))]
      for {set j 0} {$j < $nrot } {incr j} {
        set t [expr {$dthe*$j}]
        set x1 [expr $x*10.] 
        set y1 [expr $y*cos($t)*10.] 
        set z1 [expr $y*sin($t)*10.] 
 
        puts "C $x1 $y1 $z1"
      }
    }    
}
 
proc ClrCanvas {w } {
    global rmin rmax  width height ymax ymin midX midY
    global nc
    $w delete "all"
    set nc 0
    DrawAxis $w 
}
 
proc DrawAxis {w} {
 
    global rmin rmax  width height ymax ymin midX midY 
 
    set midX [expr { $width / 2 }]
    set midY [expr { $height / 2 }]
    set incrX [expr { ($width-50) /10 }]
    set incrY [expr { $height /11 }]
 
    $w create line 0 $midY  $width   $midY  -tags "Xaxis"  -fill black -width 1
    $w create line $midX 0  $midX $height  -tags "Yaxis" -fill black -width 1 
    $w create text [expr $midX-20] 20 -text "Y"
    $w create text [ expr $width-20]  [expr $midY+20]  -text "X"
}
 
proc DrawFn w {
 
    #
    # Plot the 2D egg shape using the Bakker equation 
    #
 
    global pA pE pL vol surf 
    global ex1 ex2 lam np T COLORS nCOLORS nc
    global rmin rmax  width height ymax ymin midX midY
 
    set lam [expr $pA+1.0]
    set T   [expr 1.0/($pE+1.0)]
    set ex1 [expr 1.0/(1.0+$lam)]
    set ex2 [expr $lam/(1.0+$lam)]
#    ClrCanvas .cv
 
    set divy [expr ($ymax-$ymin)/$height ]
    set divx [expr ($rmax-$rmin)/$width]   
    set divx 200
    set asp_ratio 1.6
    set ctype [lindex $COLORS [expr $nc%$nCOLORS]]
    set yp   0 
    set yp1  0    
    set xp   0 
 
    DrawAxis $w
 
    for { set n 0 } { $n <= [expr 2*$divx] } { incr n 1 } {
        set x0 [expr ($n-$divx)/($divx*1.0)]
        set x1 [expr 1.0+$x0]
        set x2 [expr 1.0-$x0]
        set  x [expr $midX+$n-$divx]
        set y [expr $midY- ($T*pow($x1,$ex1)*pow($x2,$ex2))/$divy*$asp_ratio]
        set y1 [expr $midY+ ($T*pow($x1,$ex1)*pow($x2,$ex2))/$divy*$asp_ratio]
 
        if {$n > 0} {
            .cv create line $xp $yp  $x $y    -fill $ctype   -width 2
            .cv create line $xp $yp1 $x $y1   -fill $ctype -width 2
}
set yp   $y
set yp1  $y1  
set xp   $x
}    
 
    set vol [CalcVol] 
    set surf [CalcSur] 
}
 
###############################################################################################
#
# Main with the setup up of the GUI 
###############################################################################################
 
 
# Row 1   
label $base.label#0  \
-background magenta -padx 64 -relief raised -text {Egg Calculator         (c) Danilo Roccatano 2002-2018}
label $base.label#4 \
-background magenta -padx 64 -relief raised -text {List of European Birds}
 
 
# Row 2 
label $base.label#1 -background yellow  -relief groove -text "Ellipcity:" 
 
entry $base.pE  -cursor {} -textvariable pE  -bg white
 
label $base.label#3 -background orange -relief groove -text "Lenght of the egg (cm):"
 
entry $base.pL -textvariable pL -bg white
 
# row 3
label $base.label#5  -background brown  -relief groove -text "Asymmetry:" 
 
entry $base.pA     -cursor {} -textvariable pA     -bg white
 
label $base.label#6 -background green -relief groove -text "Volume (cm^3):"
label $base.label#7 -background green -relief groove -textvariable vol 
 
label $base.label#8 -background green -relief groove -text "Surface (cm^2)"
label $base.label#9 -background green -relief groove -textvariable surf
 
canvas $base.cv -bg white -height $maxY -width $maxX
 
# row 6
 
button  $base.plot -text PLOT    -command { DrawFn .cv }  
button  $base.stl  -text STL     -command { GenSTL }  
button  $base.b0   -text "Clear" -command { ClrCanvas  .cv }
button  $base.b1   -text "EXIT" -command { exit -1}
 
text $base.t -width 50 -height 5 -wrap word  -bg gray90
 
.t insert end "Calculation of the shape of eggs using the Baker equation 
y = T*(1+x)^(1/1+lam)(1-x)^(lam/(1+lam)). 
The shape of the curve is defined by the asymmetry: A=lam-1, and the 
ellipticity: E=1/T-1, input parameters." 
 
scrollbar .s -command ".l yview"
listbox .lb -yscroll ".s set" -exportselection 1 
 
foreach item $CNAMES {
    .lb insert end $item
}
 
#
# Add contents to grid
#   
# Row 1
 
grid $base.label#0  -in $root   -row 1 -column 1  -columnspan 5 -sticky nesw
# Row 2
grid $base.label#1 -in $root    -row 2 -column 1 -sticky nesw
grid $base.pE -in $root -row 2 -column 2 -sticky nesw
grid $base.label#3 -in $root    -row 2 -column 3 -sticky nesw
grid $base.pL -in $root -row 2 -column 4 -sticky nesw
grid $base.label#4 -in $root    -row 2 -column 5 -sticky nesw
# Row 3
grid $base.label#5  -in $root   -row 3 -column 1  -sticky nesw
grid $base.pA     -in $root     -row 3 -column 2  -sticky nesw
grid $base.label#6 -in $root    -row 3 -column 3  -sticky nesw
grid $base.label#7 -in $root    -row 3 -column 4  -sticky nesw
 
grid $base.lb -in $root -row 3 -rowspan 7 -column 5  -sticky nesw
grid $base.label#8 -in $root    -row 4 -column 3  -sticky nesw
grid $base.label#9 -in $root    -row 4 -column 4  -sticky nesw
#
# Row 5 (Canvas)    
grid $base.cv -in $root -row 5 -column 1  -columnspan 4  -sticky nesw   
# Row 6 
grid $base.t    -in $root -row 6 -column 1 -columnspan 4  -sticky nesw 
# Row 7 
grid $base.b0 -in $root         -row 7 -column 1    
grid $base.plot -in $root   -row 7 -column 2 
grid $base.stl  -in $root   -row 7 -column 3 
grid $base.b1 -in $root         -row 7 -column 4    
 
# additional interface code
 
bind .cv <Double-1> { onDblClick %x %y } 
bind $base.pA  <Return> {DrawFn .cv}
bind $base.pE  <Return> {DrawFn .cv }
bind $base.pL  <Return> {DrawFn .cv }
bind .lb <Double-1> {AssignParam} 
 
update 
 
# end additional interface code
 
set width [winfo width .cv ]
set height [winfo height .cv ]
 
DrawAxis .cv